#Entrada: [3, 6, 7, 8]
#Saída: [3, 6]

input = [3,6,7,8,12]
def div3(lista)
    output = []
    for x in lista
        if(x % 3 == 0)
            output.append(x)
        end
    end
    return output
end

print(div3(input))
puts("")