#Entrada: {:chave1 => 5, :chave2 => 30, :chave3 => 20}
#Saída: [25, 900, 400]

entrada = {:chave1 => 5, :chave2 => 30, :chave3 => 20}

def powerof2(input)
    output = []
    for x in input
        output.append(x[1] ** 2)
    end
    return output
end

print(powerof2(entrada))