#Entrada: [25, 35, 45]
#Saída: ["25", "35", "45"]

entrada = [25, 35, 45]

def tostring(input)
    for x in 0..(input.length() - 1)
        input[x] = input[x].to_s()
    end
    return input
end
print(tostring(entrada))
puts("")
